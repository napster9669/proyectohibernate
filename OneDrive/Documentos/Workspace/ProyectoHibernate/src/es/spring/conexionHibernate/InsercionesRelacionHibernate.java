package es.spring.conexionHibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class InsercionesRelacionHibernate {

	public static void main(String[] args) {

		SessionFactory miFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Cliente.class)
				.addAnnotatedClass(DetallesCliente.class).buildSessionFactory();

		Session session = miFactory.openSession();

		try {

			
			  Cliente cliente1 = new Cliente("Javier", "Sanchez Perez", "Encomienda N�2");
			  DetallesCliente detallesCliente = new DetallesCliente("www.google.com",
			  "691813435", "Hola, me llamo Javier");
			  cliente1.setDetallesCliente(detallesCliente);
			  
			  session.beginTransaction();
			  
			  //la clave foranea tiene que ser la misma, sino da error //EJ: caso de id
			  //autoincrement, tiene que ser en el mismo 1, si se crea en uno 1, y en la otra
			  //tabla 2 sale un error Constraint 
			  session.save(cliente1); // guardamos en la
			  //tabla cliente y detalles_cliente
			  
			  session.getTransaction().commit();// confirmamos
			  
			  System.out.println("Regristro insertado en BBDD: " + cliente1.toString());
			 

			// Borrar cliente y tabla relacional 
			/*session.beginTransaction();

			Cliente cliente1 = session.get(Cliente.class, 1);
			
			if(cliente1 != null) {
				 session.delete(cliente1);
			}
			
			session.getTransaction().commit();
			
			if(cliente1 != null)
				System.out.println("Regristro actualiado ");
			else
				System.out.println("No existia el cliente ");
			*/
			session.close();
			
		} finally {
			miFactory.close();
		}
	}

}
