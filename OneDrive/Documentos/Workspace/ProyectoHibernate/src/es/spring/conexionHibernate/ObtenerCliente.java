package es.spring.conexionHibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ObtenerCliente {

	public static void main(String[] args) {
		SessionFactory miFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Cliente.class)
				.addAnnotatedClass(DetallesCliente.class).buildSessionFactory();

		Session session = miFactory.openSession();

		try {

			session.beginTransaction();

			DetallesCliente detalle = session.get(DetallesCliente.class, 2);

			if (detalle != null) {
				System.out.println("obtenemos detalles " + detalle);
				System.out.println("obtenemos cliente " + detalle.getCliente());
			}
			
			//Ahora vamos a eliminar en cascada
			session.delete(detalle);

			session.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
			miFactory.close();
		}

	}

}
