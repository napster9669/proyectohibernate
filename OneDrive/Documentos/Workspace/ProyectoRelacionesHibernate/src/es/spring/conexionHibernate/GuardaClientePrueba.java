package es.spring.conexionHibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class GuardaClientePrueba {

	public static void main(String[] args) {

		SessionFactory miFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Clientes.class).buildSessionFactory();
		
		Session session = miFactory.openSession();
		
		try {
			
			Clientes cliente1 = new Clientes("Javier", "Sanchez Perez", "Encomienda N�2");
			
			session.beginTransaction();
			
			session.save(cliente1); // guardamos en la tabla
			
			session.getTransaction().commit();//confirmamos 
			
			System.out.println("Regristro insertado en BBDD: " + cliente1.toString());
			
			//Lectura de registro
			
			session.beginTransaction();
			
			System.out.println("Lectura de registro con ID: " + cliente1.getId());
			
			Clientes clienteInsertado = session.get(Clientes.class, 1);
			
			System.out.println("Cliente obtenido: " + clienteInsertado.toString());
			
			session.getTransaction().commit();//confirmamos 
			
			session.close();
			
		}finally {
			
			miFactory.close();
			
			
		}
		
	}

}
