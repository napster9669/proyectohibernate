package es.spring.conexionHibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ConsultaClientes {

	public static void main(String[] args) {

		SessionFactory miFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Clientes.class)
				.buildSessionFactory();

		Session session = miFactory.openSession();

		try {

			// Comenzar session
			session.beginTransaction();

			// Consulta clientes

			List<Clientes> listClientes = session.createQuery("from Clientes").getResultList(); // Clientes es la clase
																								// que estamos manejando

			recorrerListaClientes(listClientes);

			// Consulta: dame los Juan
			listClientes = session.createQuery("from Clientes cl where cl.nombre='Juan'").getResultList(); // nombre es
																											// el
																											// atributo
																											// de la
																											// clase, no
																											// de BBDD

			recorrerListaClientes(listClientes);

		} finally {
			miFactory.close();
		}

	}

	private static void recorrerListaClientes(List<Clientes> listClientes) {
		for (Clientes cliente : listClientes) {
			System.out.println(cliente.toString());
		}
	}

}
