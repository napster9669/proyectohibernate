package es.spring.conexionHibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ActualizarClientes {

	public static void main(String[] args) {

		SessionFactory miFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Clientes.class)
				.buildSessionFactory();

		Session session = miFactory.openSession();

		try {

			// 1 forma para realizar un UPDATE
			/*
			 * int clienteId = 1;
			 * 
			 * session.beginTransaction();
			 * 
			 * Clientes cliente1 = session.get(Clientes.class, clienteId);
			 * 
			 * cliente1.setNombre("Alberto");
			 * 
			 * session.getTransaction().commit();//confirmamos
			 * 
			 * System.out.println("Regristro actualiado ");
			 */

			/*
			 * // 2 forma para UPDATE session.beginTransaction();
			 * 
			 * session.
			 * createQuery("update Clientes set Apellidos='Dominguez' where Apellidos LIKE 'P%'"
			 * ).executeUpdate();
			 * 
			 * System.out.println("Regristro actualiado ");
			 */
			
			// DELETE 
			session.beginTransaction();

			session.createQuery("delete Clientes where Apellidos = 'Dominguez'").executeUpdate();

			System.out.println("Regristro actualiado ");

			session.close();

		} finally {

			miFactory.close();

		}

	}

}
