package es.spring.pruebasHibernate;

import java.sql.Connection;
import java.sql.DriverManager;


public class pruebasJDBC {

	public static void main(String[] args) {
		
		String jdbcUrl = "jdbc:mysql://localhost:3306/pruebasHibernate?useSSL=false";
		String user = "root";
		String pass = "";

		try {
			
			System.out.println("Intentando conectar con la BBDD: " + jdbcUrl);
			
			Connection miConexConnection = DriverManager.getConnection(jdbcUrl, user, pass);
			
			System.out.println("Conexi�n exitosa");
			
		}catch (Exception e) {
			System.out.println("Error en la conexion");
			e.printStackTrace();
		}
		
		
	}

}
